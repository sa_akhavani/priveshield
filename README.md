# Browser Profile Manager

### How To Build
- install Node.js
- go to the project directory  
- run `npm install -g yarn`
- run `yarn`
- run `rm -r build; yarn build`
- open Chrome -> More Tools -> Extensions. Enable Developer Mode. -> Load unpacked -> Select `build` directory.

### Features

- ```New Tab``` New Tabs are created with a new `tmp profile` unless there is already an active profile enabled.
- ```Active Tab``` When a tab is selected, the `switch profile` happens. The previous active tab's data gets stored into
  memory. New tab's profile's cookies and storage data loads into memory.
  We cannot change an open tab's profile manually.
- ```Close Tab```. If tab's profile is tmp, remove profile data.
- ```Persistence Detection```. If a tab contains a login form, switch its profile to persistent/social.
  (not implemented yet)

### Todo
- Don't close browser if all tabs closed (on login detected issue)
- Fix auto reload issue after deleting all cookies ***
- handle active profile

### Ideas

- Can change headers to prevent fingerprinting 

### Good Resources
- https://www.youtube.com/watch?v=Ipa58NVGs_c&ab_channel=KyleRobinsonYoung
- https://github.com/shama/letswritecode/tree/master/how-to-make-chrome-extensions
- https://github.com/AlexandraKreidich/SwitchGithubAccounts/blob/master
- Ghostery Extension Source Code
  
### Similar Extensions
- https://github.com/emerysteele/CookieProfileSwitcher
- https://github.com/Aminadav/swap-my-cookies-multisite/tree/master/js
- https://www.youtube.com/watch?v=Gy7lyvAfOSw
