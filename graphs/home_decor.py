import networkx as nx
# import pyvis
from pyvis.network import Network
net = Network("1300px", "1300px", notebook=True, font_color="white", bgcolor="white")
G = nx.Graph()

#lamp - wayfair - money
first_parties = [(1, {"color": "blue", "value": 40, "shape": "circle", "label": "LampPlus", "labelHighlightBold": True}),
(2, {"color": "blue", "value": 40, "shape": "circle", "label": "Wayfair", "labelHighlightBold": True}),
(3, {"color": "blue", "value": 40, "shape": "circle", "label": "MoneyControl", "labelHighlightBold": True})
]
G.add_nodes_from(first_parties)

# lampplus
# 101 unique trackers
# 2 insider
uniques = []
edges = []
for i in range(11, 111):
    uniques.append((i, {"color": "green", "value": 10, "shape": "triangle"}))
    edges.append([1, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)
G.add_edges_from([(12, 13), (14, 15)])



# wayfair
# 47 unique
# 0 insider
uniques = []
edges = []
for i in range(201, 247):
    uniques.append((i, {"color": "green", "value": 10, "shape": "triangle"}))
    edges.append([2, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)



# moneycontroll
# 91 unique
# 5 insider
uniques = []
edges = []
for i in range(301, 391):
    uniques.append((i, {"color": "green", "value": 10, "shape": "triangle"}))
    edges.append([3, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)
G.add_edges_from([(301, 302), (302, 303), (303, 304), (305, 306), (307, 308)])




shared = []
# 5 with lamp - money controll
for i in range(500, 505):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([3, i])
G.add_nodes_from(shared)
G.add_edges_from(edges)
# G.add_edges_from([(20, 350), (21, 351), (22, 352), (23, 353), (24, 353)])

# 6 with lamp - wayfair
for i in range(510, 516):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([2, i])
G.add_nodes_from(shared)
G.add_edges_from(edges)
# G.add_edges_from([(30, 210), (31, 211), (32, 212), (33, 213), (34, 213), (35, 214)])


# 4 wayfair - money (1 is green)
for i in range(520, 524):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([2, i])
    edges.append([3, i])
G.add_nodes_from(shared)
G.add_edges_from(edges)
# G.add_edges_from([(220, 360), (221, 362), (222, 363), (223, 367)])


# 8 with lamp - money - wayfair
for i in range(530, 538):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([3, i])
    edges.append([2, i])
G.add_nodes_from(shared)
G.add_edges_from(edges)
# G.add_edges_from([(50, 370), (51, 371), (52, 373), (53, 374), (54, 375), (55, 376), (56, 377), (57, 378)])
# G.add_edges_from([(50, 230), (51, 231), (52, 232), (53, 233), (54, 234), (55, 235), (56, 236), (57, 237)])
# G.add_edges_from([(370, 230), (371, 231), (373, 232), (374, 233), (375, 234), (376, 235), (377, 236), (378, 237)])


net.from_nx(G)
# net.show_buttons(filter_=['physics'])
net.show_buttons()
# net.set_options("""
# const options = {
#   "nodes": {
#     "borderWidth": null,
#     "borderWidthSelected": null,
#     "opacity": null,
#     "font": {
#       "size": 32,
#       "strokeWidth": 1
#     },
#     "size": null
#   },
#   "edges": {
#     "color": {
#       "inherit": true
#     },
#     "selfReferenceSize": null,
#     "selfReference": {
#       "angle": 0.7853981633974483
#     },
#     "smooth": {
#       "forceDirection": "none"
#     }
#   },
#   "physics": {
#     "minVelocity": 0.75
#   }
# }
# """)
net.show("home_decor.html")
