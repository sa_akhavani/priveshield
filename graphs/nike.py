import networkx as nx
from pyvis.network import Network
net = Network("1300px", "1300px", notebook=True, font_color="white", bgcolor="white")
G = nx.Graph()

#Nike 74 uniques

first_parties = [(1, {"color": "blue", "value": 40, "shape": "circle", "label": "Nike", "labelHighlightBold": True})]
G.add_nodes_from(first_parties)

# lampplus
# 101 unique trackers
# 1 insider
uniques = []
edges = []
for i in range(11, 111):
    uniques.append((i, {"color": "green", "size": 10, "shape": "triangle"}))
    edges.append([1, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)
G.add_edges_from([(12, 13)])



net.from_nx(G)
# net.show_buttons(filter_=['physics'])
net.show_buttons()
# net.set_options("""
# const options = {
#   "nodes": {
#     "borderWidth": null,
#     "borderWidthSelected": null,
#     "opacity": null,
#     "font": {
#       "size": 25,
#       "strokeWidth": 1
#     },
#     "size": null
#   },
#   "edges": {
#     "color": {
#       "inherit": true
#     },
#     "selfReferenceSize": null,
#     "selfReference": {
#       "angle": 0.7853981633974483
#     },
#     "smooth": {
#       "forceDirection": "none"
#     }
#   },
#   "physics": {
#     "minVelocity": 0.75
#   }
# }
# """)
net.show("nike.html")
