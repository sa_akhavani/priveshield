import networkx as nx
# import pyvis
from pyvis.network import Network
net = Network("1300px", "1300px", notebook=True, font_color="white", bgcolor="white")
G = nx.Graph()

#lamp - wayfair - money
first_parties = [(1, {"color": "blue", "value": 40, "shape": "circle", "label": "Nike", "labelHighlightBold": True}),
(2, {"color": "blue", "value": 40, "shape": "circle", "label": "Adidas", "labelHighlightBold": True}),
(3, {"color": "blue", "value": 40, "shape": "circle", "label": "Reebok", "labelHighlightBold": True}),
(4, {"color": "blue", "value": 40, "shape": "circle", "label": "Nypost", "labelHighlightBold": True})
]
G.add_nodes_from(first_parties)

# nike
# 51 unique trackers
# 0 insider
uniques = []
edges = []
for i in range(11, 61):
    uniques.append((i, {"color": "green", "value": 10, "shape": "triangle"}))
    edges.append([1, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)



# wayfair
# 29 unique
# 1 insider
uniques = []
edges = []
for i in range(201, 229):
    uniques.append((i, {"color": "green", "value": 10, "shape": "triangle"}))
    edges.append([2, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)
G.add_edges_from([(201, 202)])


# reebok
# 61 unique
# 1 insider
uniques = []
edges = []
for i in range(301, 361):
    uniques.append((i, {"color": "green", "value": 10, "shape": "triangle"}))
    edges.append([3, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)
G.add_edges_from([(301, 302)])


# NyPost
# 204 unique
# 18 insider
uniques = []
edges = []
for i in range(401, 604):
    uniques.append((i, {"color": "green", "value": 10, "shape": "triangle"}))
    edges.append([4, i])

G.add_nodes_from(uniques)
G.add_edges_from(edges)
G.add_edges_from([(401, 402), (402, 403), (403, 404), (404, 405), (405, 406), (406, 407), (407, 408), (408, 409), (409, 410), (410, 411), (411, 412), (412, 413), (413, 414), (414, 415), (415, 416), (416, 417), (417, 418), (418, 419)])



shared = []
# 11 with reebok - nike
for i in range(701, 711):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([3, i])
# G.add_nodes_from(shared)
# G.add_edges_from(edges)


# 1 with adidas - nike
for i in range(711, 711):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([2, i])
# G.add_nodes_from(shared)
# G.add_edges_from(edges)



# 31 adidas - reebok
for i in range(721, 751):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([2, i])
    edges.append([3, i])
# G.add_nodes_from(shared)
# G.add_edges_from(edges)



# 11 with adidas - nike - reebok
for i in range(761, 771):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([3, i])
    edges.append([2, i])
# G.add_nodes_from(shared)
# G.add_edges_from(edges)

# 1-nike 2-adidas 3-reebok 4-nypost
# 1 nike - nypost
for i in range(781, 781):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([4, i])
# G.add_nodes_from(shared)
# G.add_edges_from(edges)

# 3 adidas - nypost
for i in range(791, 793):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([2, i])
    edges.append([4, i])


# 14 reebok - nypost
for i in range(801, 814):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([3, i])
    edges.append([4, i])

# 4 reebok - nike - nypost
for i in range(821, 824):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([3, i])
    edges.append([4, i])

# 3 reebok - adidas - nypost
for i in range(831, 833):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([2, i])
    edges.append([4, i])


# 8 reebok - adidas - nypost - nike
for i in range(841, 848):
    shared.append((i, {"color": "red", "value": 10, "shape": "triangle"}))
    edges.append([1, i])
    edges.append([2, i])
    edges.append([3, i])
    edges.append([4, i])


G.add_nodes_from(shared)
G.add_edges_from(edges)


net.from_nx(G)
net.show_buttons()
# net.set_options("""
# const options = {
#   "nodes": {
#     "borderWidth": null,
#     "borderWidthSelected": null,
#     "opacity": null,
#     "font": {
#       "size": 35,
#       "strokeWidth": 1
#     },
#     "size": null
#   },
#   "edges": {
#     "color": {
#       "inherit": true
#     },
#     "selfReferenceSize": null,
#     "selfReference": {
#       "angle": 0.7853981633974483
#     },
#     "smooth": {
#       "forceDirection": "none"
#     }
#   },
#   "physics": {
#     "minVelocity": 0.75
#   }
# }
# """)
net.show("running_shoe.html")
