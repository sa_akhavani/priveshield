import cloudscraper
from bs4 import BeautifulSoup
from googletrans import Translator
import os
from google.cloud import language_v1
from google.cloud.language_v1.gapic import enums
from google.cloud.language_v1 import types
from google.cloud import language

website = 'https://www.amazon.com/'

def scrape_contents():

    scraper = cloudscraper.create_scraper() 
    headers = {'user-agent': 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
     
    try:
        r = scraper.get(website, headers = headers)
     
        soup = BeautifulSoup(r.text, 'html.parser')
        title = soup.find('title').text
        description = soup.find('meta', attrs={'name': 'description'})
     
        if "content" in str(description):
            description = description.get("content")
        else:
            description = ""
     
     
        h1 = soup.find_all('h1')
        h1_all = ""
        for x in range (len(h1)):
            if x ==  len(h1) -1:
                h1_all = h1_all + h1[x].text
            else:
                h1_all = h1_all + h1[x].text + ". "
     
     
        paragraphs_all = ""
        paragraphs = soup.find_all('p')
        for x in range (len(paragraphs)):
            if x ==  len(paragraphs) -1:
                paragraphs_all = paragraphs_all + paragraphs[x].text
            else:
                paragraphs_all = paragraphs_all + paragraphs[x].text + ". "
     
     
     
        h2 = soup.find_all('h2')
        h2_all = ""
        for x in range (len(h2)):
            if x ==  len(h2) -1:
                h2_all = h2_all + h2[x].text
            else:
                h2_all = h2_all + h2[x].text + ". "
     
     
     
        h3 = soup.find_all('h3')
        h3_all = ""
        for x in range (len(h3)):
            if x ==  len(h3) -1:
                h3_all = h3_all + h3[x].text
            else:
                h3_all = h3_all + h3[x].text + ". "
     
        allthecontent = ""
        allthecontent = str(title) + " " + str(description) + " " + str(h1_all) + " " + str(h2_all) + " " + str(h3_all) + " " + str(paragraphs_all)
        allthecontent = str(allthecontent)[0:999]

        return allthecontent
     
    except Exception as e:
        print(e)


def translate_contents(contents):
    translator = Translator()
    try:
        translation = translator.translate(contents).text
        translation = str(translation)[0:999]
        return translation
        # time.sleep(10)
        
    except Exception as e:
        print(e)


 
def categorize(contents):
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/ali/Desktop/seclab/profile-manager/browser-profile-manager/scripts/browser-profile-manager-a28276a2d00d.json'
     
    try:
            text_content = str(contents)
            text_content = text_content[0:1000]
     
            client = language_v1.LanguageServiceClient()
     
            # type_ = language_v1.enums.Document.Type.PLAIN_TEXT
            # type_ = language_v1.Document.Type.PLAIN_TEXT
            # language = "en"
            # document = {"content": text_content, "type": 'PLAIN_TEXT', "language": language}
            # encoding_type = enums.EncodingType.UTF8
            
            document = language_v1.types.Document(
                content = text_content,
                language = 'en',
                type_= 1
            )
            
            response = client.classify_text(document)
            print('res:')
            print(response)
            print(response.categories[0].name)
            print(str(int(round(response.categories[0].confidence,3)*100))+"%")
     
     
    except Exception as e:
        print(e)



if __name__ == '__main__':

    contents = scrape_contents()    
    print(contents)
    # contents = translate_contents(contents)
    categorize(contents)

