module.exports = [{
    name: "running_shoes",
    urls: [
        'https://www.nike.com/',
        'https://www.nike.com/t/air-zoom-victory-tour-2-golf-shoes-75nbTz/DJ6569-105',
        'https://www.nike.com/t/air-zoom-terra-kiger-7-mens-trail-running-shoes-9tC16Z/CW6062-800',
        'https://www.nike.com/t/flex-experience-run-10-mens-running-shoes-BNSNRc/CI9960-400',
        'https://www.reebok.com/us',
        'https://www.reebok.com/us/goodr-floatride-energy-3-men-s-shoes/GX0280.html',
        'https://www.reebok.com/us/floatride-run-fast-3-men-s-running-shoes/FW9630.html',
        'https://www.reebok.com/us/zig-dynamica-2-mens-shoes/GY9564.html',
        'https://www.adidas.com/us',
        'https://www.adidas.com/us/runfalcon-2.0-shoes/FZ2808.html',
        'https://www.adidas.com/us/4dfwd-shoes/Q46445.html',
        'https://www.adidas.com/us/4dfwd-pulse-shoes/Q46451.html'
    ]
}, {
    name: "clothing",
    urls: [
        'https://www2.hm.com/en_us/index.html',
        'https://www2.hm.com/en_us/productpage.0996490002.html',
        'https://www2.hm.com/en_us/productpage.1001389001.html',
        'https://www2.hm.com/en_us/productpage.0849214008.html',
        'https://gap.com/',
        'https://www.gap.com/browse/product.do?pid=715087022&cid=1066503&pcid=1066503&vid=1&cpos=9&cexp=1567&kcid=CategoryIDs%3D1066503&ctype=Listing&cpid=res21081019700086491042788&ccam=8323#pdp-page-content',
        'https://www.gap.com/browse/product.do?pid=715087042&cid=1066503&pcid=1066503&vid=1&cpos=2&cexp=1567&kcid=CategoryIDs%3D1066503&ctype=Listing&cpid=res21081019700086491042788&ccam=8323#pdp-page-content',
        'https://www.gap.com/browse/product.do?pid=870637002&rrec=true&mlink=5001,1,dynamicerror_gapoos1_rr_9&clink=1#pdp-page-content',
        'https://gucci.com/us/en/',
        'https://www.gucci.com/us/en/pr/men/ready-to-wear-for-men/denim-for-men/gg-reversible-denim-jacket-p-702763XDB2K4759',
        'https://www.gucci.com/us/en/pr/men/ready-to-wear-for-men/denim-for-men/ripped-eco-bleached-organic-denim-pant-p-623953XDBFM4009',
        'https://www.gucci.com/us/en/pr/men/ready-to-wear-for-men/denim-for-men/tapered-eco-stonewashed-denim-pant-p-408637XDBK81000',
        'https://macys.com',
        'https://www.macys.com/shop/product/nautica-mens-sustainably-crafted-classic-fit-deck-polo-shirt?ID=10400567&CategoryID=20626',
        'https://www.macys.com/shop/product/mens-kasum-short-sleeve-t-shirt?ID=12558361&CategoryID=20626',
        'https://www.macys.com/shop/product/tommy-bahama-mens-hula-all-day-shirt?ID=13778595&CategoryID=20626'
    ]
}, {
    name: 'watch',
    urls: [
        'https://www.swatch.com/en-us/homepage',
        'https://www.swatch.com/en-us/c-grey-sb03m100/SB03M100.html',
        'https://www.swatch.com/en-us/die-glocke-gb285/GB285.html',
        'https://www.swatch.com/en-us/golden-tac-gb274/GB274.html',
        'https://www.shinola.com/',
        'https://www.shinola.com/mens/watches/the-runwell-41-leather-band-7728.html',
        'https://www.shinola.com/mens/watches/sea-creatures-40mm.html#sku=20225332-sdt-010976515',
        'https://www.shinola.com/mens/watches/duck-watch.html#sku=20211229-sdt-007620705',
        'https://www.tissotwatches.com/en-us/?gclid=EAIaIQobChMIntTfz7G78gIVDpSzCh1tuQWWEAAYASAAEgK1JvD_BwE',
        'https://www.tissotwatches.com/en-us/t1214204705103.html',
        'https://www.tissotwatches.com/en-us/catalog/product/view/id/2583/s/t1204171109101/',
        'https://www.tissotwatches.com/en-us/t1204171105101.html',
        'https://www.rolex.com/',
        'https://www.rolex.com/watches/cosmograph-daytona.html',
        'https://www.rolex.com/watches/sky-dweller.html',
        'https://www.rolex.com/watches/submariner.html',
        'https://www.omegawatches.com/en-us/',
        'https://www.omegawatches.com/en-us/watches/seamaster/diver-300-m/product',
        'https://www.omegawatches.com/en-us/watches/seamaster/heritage-models/product',
        'https://www.omegawatches.com/en-us/watches/constellation/constellation/product'
    ]
}, {
    name: 'jewelry',
    urls: [
        'https://us.louisvuitton.com/eng-us/homepage',
        'https://us.louisvuitton.com/eng-us/products/b-blossom-studs-yellow-gold-white-gold-onyx-and-diamonds-nvprod2900102v',
        'https://us.louisvuitton.com/eng-us/products/b-blossom-pendant-yellow-gold-white-gold-malachite-and-diamonds-nvprod2900095v',
        'https://us.louisvuitton.com/eng-us/products/b-blossom-studs-yellow-gold-white-gold-onyx-and-diamonds-nvprod2900102v',
        'https://www.swarovski.com/en-US/',
        'https://www.swarovski.com/en-US/p-M5600794/Somnia-necklace-Green-Gold-tone-plated/?variantID=5601906',
        'https://www.swarovski.com/en-US/p-5562083/Tennis-Deluxe-Mixed-Strandage-White-Rhodium-plated/',
        'https://www.swarovski.com/en-US/p-M5610804/Dulcis-cocktail-ring-Green/?variantID=5609725',
        'https://www.mikimotoamerica.com/us_en/',
        'https://www.mikimotoamerica.com/us_en/akoya-cultured-pearl-pendant-white-gold',
        'https://www.mikimotoamerica.com/us_en/akoya-cultured-pearl-necklace-earring-set-white-gold-2',
        'https://www.mikimotoamerica.com/us_en/ocean-strand-necklace',
        'https://www.tiffany.com/',
        'https://www.tiffany.com/jewelry/necklaces-pendants/tiffany-hardwear-freshwater-pearl-necklace-in-sterling-silver-32-64047965/',
        'https://www.tiffany.com/jewelry/necklaces-pendants/tiffany-victoria-diamond-vine-circle-pendant-in-platinum-63919152/',
        'https://www.tiffany.com/jewelry/necklaces-pendants/elsa-peretti-color-by-the-yard-sprinkle-necklace-25841182/'
    ]
}, {
    name: "cars",
    urls: [
        'https://mbusa.com',
        'https://www.mbusa.com/en/vehicles/class/gle/suv',
        'https://www.mbusa.com/en/vehicles/class/gla/suv',
        'https://www.mbusa.com/en/special-offers?class=GLA:SUV',
        'https://ford.com',
        'https://www.ford.com/suvs-crossovers/escape/models/escape-se-plug-in-hybrid/?gnav=header-electrified-vhp',
        'https://www.ford.com/cars/fusion/',
        'https://www.ford.com/cars/mustang/models/ecoboost-fastback/',
        'http://volvo.com',
        'https://www.volvocars.com/us/v/cars/c40-electric',
        'https://www.volvocars.com/us/v/cars/s90-hybrid',
        'https://www.volvocars.com/us/v/cars/s90-hybrid/shop',
        'https://nissanusa.com',
        'https://www.nissanusa.com/vehicles/sports-cars/370z-coupe.html',
        'https://www.nissanusa.com/vehicles/crossovers-suvs/kicks.html',
        'https://www.nissanusa.com/shopping-tools/build-price?models=kicks&modelYear=current-year',
        'https://jeep.com',
        'https://www.jeep.com/gladiator.html',
        'https://www.jeep.com/bmo.gladiator.2021.html#/models/2021/gladiator?app=bmo&pageType=vehiclepage&vehicle=gladiator&year=2021',
        'https://www.jeep.com/cherokee.html',
    ]
}, {
    name: "computer_equipment",
    urls: [
        'https://acer.com',
        'https://www.acer.com/ac/en/US/content/series/swift5',
        'https://www.acer.com/ac/en/US/content/professional-series/acerchromebookenterprisespin713cp7133w',
        'https://store.acer.com/en-us/24-aopen-cl1-series-monitor-24cl1y-bi',
        'https://hp.com',
        'https://www.hp.com/us-en/shop/pdp/victus-by-hp-laptop-16-e0097nr',
        'https://www.hp.com/us-en/shop/pdp/hp-wireless-mouse-z3700-p-7uh88aa-abl-1',
        'https://www.hp.com/us-en/shop/pdp/hp-spectre-x360-2-in-1-laptop-14-ef0797nr',
        'https://dell.com',
        'https://www.dell.com/en-us/shop/dell-laptops/sf/xps-laptops',
        'https://www.dell.com/en-us/shop/dell-laptops/alienware-m17-r5-gaming-laptop/spd/alienware-m17-r5-amd-gaming-laptop/wnm17r5fpcjs',
        'https://www.dell.com/en-us/shop/deals-for-business/vostro-3510-laptop/spd/vostro-15-3510-laptop/smv153w11p1c8004',
        'https://bestbuy.com',
        'https://www.bestbuy.com/site/apple-10-9-inch-ipad-air-latest-model-4th-generation-with-wi-fi-64gb-sky-blue/5985620.p?skuId=5985620',
        'https://www.bestbuy.com/site/macbook-air-13-3-laptop-apple-m1-chip-8gb-memory-256gb-ssd-latest-model-space-gray/5721600.p?skuId=5721600',
        'https://www.bestbuy.com/site/macbook-pro-13-3-laptop-apple-m1-chip-8gb-memory-256gb-ssd-latest-model-space-gray/6418601.p?skuId=6418601'
    ]
}, {
    name: "banking",
    urls: [
        'https://chase.com',
        'https://creditcards.chase.com/?CELL=6TKV',
        'https://personal.chase.com/personal/checking',
        'https://creditcards.chase.com/travel-credit-cards/marriott-bonvoy/boundless?CELL=6TKV',
        'https://americanexpress.com',
        'https://www.americanexpress.com/us/credit-cards/?intlink=us-en-hp-product1-personal-personalcards',
        'https://www.americanexpress.com/us/credit-cards/card/gold-card/?eep=25330&linknav=US-Acq-Shop-Consumer-VAC-Prospect-ViewCardDetail-GoldCard',
        'https://www.americanexpress.com/en-us/banking/online-savings/account/?extlink=us-en-hp-product2-Internalchannel-SavingsAccount-Nav'
    ]
}, {
    name: "insurance",
    urls: [
        'https://libertymutual.com',
        'https://www.libertymutual.com/property-insurance/homeowners',
        'https://business.libertymutual.com/',
        'https://www.libertymutual.com/vehicle/auto-insurance',
        'https://geico.com',
        'https://www.geico.com/living/',
        'https://www.geico.com/about/organization-discount/',
        'https://www.geico.com/living/',
        'https://progressive.com',
        'https://www.progressive.com/auto/',
        'https://www.progressive.com/auto/discounts/snapshot/',
        'https://www.progressive.com/auto/discounts/snapshot/'
    ]
}, {
    name: "streaming_service",
    urls: [
        'https://hbomax.com',
        'https://www.hbomax.com/same-day-premieres',
        'https://www.hbomax.com/plans/duration',
        'https://www.hbomax.com/subscribe/plan-picker',
        'https://disneyplus.com',
        'https://help.disneyplus.com/csp?id=csp_article_content&sys_kb_id=db1dea5b1bc101944bf98517624bcb52',
        'https://www.disneyplus.com/sign-up?type=standard',
        'https://www.disneyplus.com/legal/your-california-privacy-rights',
        'https://www.spotify.com/us/',
        'https://www.spotify.com/us/premium/',
        'https://www.spotify.com/us/about-us/contact/',
        'https://artists.spotify.com/home?_ga=2.93943005.1331861507.1629317254-482035993.1629317254'
    ]
}, {
    name: "marketplace",
    urls: [
        'https://amazon.com',
        'https://www.amazon.com/s?k=fire+tv+stick&i=electronics&rh=n%3A172282%2Cp_89%3AAmazon%2Cp_6%3AATVPDKIKX0DER&pd_rd_r=7a3f31c0-7bd5-4883-8a22-2a57cbcb95fb&pd_rd_w=lK6vF&pd_rd_wg=sIHXb&pf_rd_p=32ef19ea-cf4a-4645-8c4e-cb5e5dc6fe8b&pf_rd_r=VKDVBPS7A2VPX574QRRN&qid=1610039565&rnid=303116011&ref=pd_gw_unk',
        'https://www.amazon.com/Fire-TV-Stick-4K-with-Alexa-Voice-Remote/dp/B079QHML21/ref=sr_1_2?dchild=1&keywords=fire+tv+stick&m=ATVPDKIKX0DER&pd_rd_r=7a3f31c0-7bd5-4883-8a22-2a57cbcb95fb&pd_rd_w=lK6vF&pd_rd_wg=sIHXb&pf_rd_p=32ef19ea-cf4a-4645-8c4e-cb5e5dc6fe8b&pf_rd_r=VKDVBPS7A2VPX574QRRN&qid=1628649217&refinements=p_89%3AAmazon%2Cp_6%3AATVPDKIKX0DER&rnid=303116011&s=electronics&sr=1-2',
        'https://www.amazon.com/Echo-Dot/dp/B07FZ8S74R/ref=zg_bs_electronics_home_2?_encoding=UTF8&psc=1&refRID=G7ZGTCWNJ3KZRMRWBFRA',
        'https://ebay.com',
        'https://www.ebay.com/b/Home-Decor/10033/bn_1849733',
        'https://www.ebay.com/itm/182456147690?_trkparms=%26rpp_cid%3D5e0a46904a2b0f703bccc44b%26rpp_icid%3D5e0a46904a2b0f703bccc44a',
        'https://www.ebay.com/itm/193864314502',
        'https://walmart.com',
        'https://www.walmart.com/ip/Little-Tikes-Easy-Store-Kids-Picnic-Table-with-Umbrella/23554620',
        'https://www.walmart.com/ip/Cuisinart-3-In-1-Stainless-Five-Burner-Propane-Gas-Grill-with-Side-Burner/273331077',
        'https://www.walmart.com/ip/Contixo-F30-4K-UHD-Drone-for-Adults-with-Wifi-Camera-GPS-FPV-Follow-Me-Two-Batteries/439326620?wpa_bd=&wpa_pg_seller_id=44559F3AE39542E588DEFBC4C0CBA137&wpa_ref_id=wpaqs:it3dewrreZlUGhbbsdDk15mNv7AlgGADVsYvysQxdhLMdrufZqLqdTggAZFCx1i4of4dTBYCXST6Zmj1UVUiOzpt9faDUqcYNjaImK-4sH1IUM0JKQh0veFwzsvCnM0r3aFZCMVRxXNGYPCn89XFvdme__sOrW75OzH6ZRUOXCUvIwaFtR3UfAoKXP0oI2splNjY_3WoYkVJIUxhnI3EXQ&wpa_tag=&wpa_aux_info=&wpa_pos=5&wpa_plmt=1145x1145_T-C-IG_TI_1-6_HL-INGRID-GRID-DEALS-NY&wpa_aduid=f50f4996-b1c8-4c05-b8e3-c6dc4ae95a5c&wpa_pg=deals&wpa_pg_id=3444880&wpa_st=Savings%2BSpotlight&wpa_tax=1072864_1045879&wpa_bucket=__bkt__',
        'https://aliexpress.com',
        'https://sale.aliexpress.com/__pc/rank_detail.htm?spm=a2g0o.home.ranking-center.5.650c2145zAa0Qy&rankId=11309471&rankCategoryId=7&labelId=&leafCateId=200001086&rankType=TOP_IDEAL_2020&tagId=manual_global_715874444556231969_200001086&requestFrom=other&productIds2Top=1005001593477046&scm=1007.30282.220441.0&scm_id=1007.30282.220441.0&scm-url=1007.30282.220441.0&pvid=7f423bf1-69cd-4484-a476-1f23b371db39&floorId=6600008',
        'https://www.aliexpress.com/item/4001179998753.html?spm=a2g0o.ams_97944.topranking.1.6dffF8eSF8eS2W&pdp_ext_f=%7B%22ship_from%22:%22ES%22,%22sku_id%22:%2210000015059918753%22%7D&scm=1007.26694.226824.0&scm_id=1007.26694.226824.0&scm-url=1007.26694.226824.0&pvid=106201a0-4387-4c92-aedb-8c8d191a66a4&fromRankId=11309471&_t=fromRankId:11309471',
        'https://www.aliexpress.com/item/1005003076995094.html?spm=a2g0o.ams_97944.topranking.1.1ea7XpSsXpSsVX&pdp_ext_f=%7B%22sku_id%22:%2212000023911723627%22%7D&scm=1007.26694.226824.0&scm_id=1007.26694.226824.0&scm-url=1007.26694.226824.0&pvid=95a0dcc2-1275-4dbb-bbb0-50d10a9e632e&fromRankId=1756850&_t=fromRankId:1756850',
        'https://target.com',
        'https://www.target.com/p/31-stackable-shelf-room-essentials/-/A-77372183',
        'https://www.target.com/p/6-shelf-hanging-closet-organizer-gray-room-essentials-8482/-/A-14914512',
        'https://www.target.com/p/storage-ottoman-room-essentials/-/A-82102719',
    ]
}, {
    name: 'home_decor',
    urls: [
        'https://wayfair.com',
        'https://www.wayfair.com/decor-pillows/pdp/willa-arlo-interiors-2175-glass-wall-sconce-w004931487.html',
        'https://www.wayfair.com/decor-pillows/pdp/union-rustic-poulan-4-piece-coffee-table-tray-set-w005933304.html',
        'https://www.wayfair.com/kitchen-tabletop/pdp/three-posts-wickham-16-piece-dinnerware-set-service-for-4-w001662564.html',
        'https://lampsplus.com',
        'https://www.lampsplus.com/products/possini-euro-deco-12-and-three-quarter-inch-wide-warm-brass-ceiling-light__1f902.html',
        'https://www.lampsplus.com/products/eagleton-13-and-one-half-inch-wide-oil-rubbed-bronze-led-ceiling-light__35f49.html',
        'https://www.lampsplus.com/products/possini-euro-double-drum-18-inch-wide-white-ceiling-light__p0197.html'
    ]
}, {
    name: 'health',
    urls: [
        'https://cvs.com',
        'https://www.cvs.com/shop/cvs-health-automatic-blood-pressure-monitor-prodid-800231',
        'https://www.cvs.com/shop/cvshealth-series-800-upper-arm-blood-pressure-monitor-prodid-800229',
        'https://www.cvs.com/shop/omron-3-series-upper-arm-blood-pressure-monitor-bp7100-prodid-1012348'
    ]
}, {
    name: "travel",
    urls: [
        'https://marriott.com',
        'https://www.marriott.com/loyalty/earn/credit-card-rewards.mi',
        'https://www.marriott.com/loyalty/member-benefits.mi',
        'https://www.marriott.com/explore/findDestinations.mi?theme=family&maxDuration=1',
        'https://hilton.com',
        'https://travel.hilton.com/en_us/offers/freenight/?adobe_mc=26047352337883267041584646303468396027&cid=OH,MB,EntHPExpGrid,MULTIPR,Offer,Home,SingleLink,i81609',
        'https://hiltonbyplaya.com/rose-hall?utm_source=Hilton&utm_medium=link+off&utm_term=Hilton+Rose+Hall&utm_campaign=Hilton+Link+Off',
        'https://hiltonbyplaya.com/rose-hall/family-suites',
        'https://southwest.com',
        'https://www.southwestvacations.com/?utm_source=southwest&utm_medium=psite-homepage&utm_campaign=southwest_psite-homepage&utm_content=D!polaroid_L!multi&utm_term=P!q219bundlesave&clk=4870063&cbid=4870063#!/',
        'https://www.southwesthotels.com/apartments/index.en-us.html?aid=961823;label=SWA-GSUBNAV-OTHER-HOTELS;sid=5b289e6904b3acb34ebb27330a4aa04e;inac=0&',
        'https://www.southwesthotels.com/dealspage.en-us.html?aid=961823&label=SWA-GSUBNAV-SPECIALOFFERS-HOTELS&lang=en-us',
        'https://aa.com',
        'https://www.aavacations.com/en/vacation-packages-to-europe?reportedLocation=HomePageB3&reportedTitle=&_locale=en_US#',
        'https://www.aa.com/i18n/travel-info/at-the-airport.jsp?anchorEvent=false&from=Nav',
        'https://www.aa.com/i18n/travel-info/check-in-and-arrival.jsp',
        'https://united.com',
        'https://www.united.com/ual/en/us/fly/travel/restrictions-map.html',
        'https://www.united.com/en/us/fly/travel/credit.html',
        'https://www.united.com/en/us/manageres/mytrips',
        'https://jetblue.com',
        'https://www.jetblue.com/flying-with-us/our-planes',
        'https://www.jetblue.com/flying-with-us/our-planes/A321-long-range',
        'https://www.jetblue.com/our-company',
    ]
}, {
    name: "rental_car",
    urls: [
        'https://enterprise.com',
        'https://www.efleets.com/en/locations.html?mcid=websiteredirect:41819948&cm_mmc=EnterpriseWebsite-_-Header-_-rent.efleet-_-en_US',
        'https://www.enterprisecarsales.com/locations/?utm_source=Enterprise&utm_medium=Referral&utm_campaign=NavLoc&cm_mmc=EnterpriseWebsite-_-Header-_-carsales.locator-_-ENUS&mcid=websiteredirect:41819955',
        'https://www.enterprisecarshare.com/us/en/locations.html?mcid=websiteredirect:41819960&cm_mmc=EnterpriseWebsite-_-Header-_-locations.carshare-_-ENUS',
        'https://rentalcars.com',
        'https://www.rentalcars.com/us/city/us/los-angeles/',
        'https://www.rentalcars.com/us/city/us/chicago/',
        'https://www.rentalcars.com/us/city/us/atlanta/',
        'https://hertz.com',
        'https://www.hertz.com/rentacar/misc/index.jsp?targetPage=gold_plus_rewards_overview.jsp',
        'https://www.hertz.com/rentacar/rental-car-deals/hertz-local-edition',
        'https://www.hertz.com/rentacar/emember/rewards-list/loyalty-redemption-points-program',
        'https://budget.com',
        'https://www.budget.com/en/offers/us-offers',
        'https://www.budget.com/en/offers/us-offers/35-off-car-rental?ICID=bg-all-210706-op-s2-nextgreatmemory&utm_campaign=next-great-memory-2021&utm_content=op-s2-nextgreatmemory',
        'https://www.budget.com/en/cars/vehicles/us',
        'https://avis.com',
        'https://www.avis.com/en/offers/partners/amazon-benefits?ICID=av-all-210526-non-amp-hp-s5-avisamazon&utm_campaign=amazon2021&utm_content=non-amp-hp-s5-amazon2021',
        'https://www.avis.com/en/offers/us-offers/last-minute-travel-deals?ICID=av-all-200529-hp-s4-lastminutedog&utm_campaign=lastminutedog',
        'https://www.avis.com/en/offers/us-offers/weekly-car-rental-deal?ICID=av-all-210316-op-last-minute-s3-weekly&utm_campaign=weekly&utm_content=op-last-minute-s3-weekly'
    ]
}]