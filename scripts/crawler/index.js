// based on https://gist.github.com/glenhallworthreadify/d447e9d6b1fc9cb807b46f952236d4bc
// https://gist.github.com/tegansnyder/c3aeae4d57768c58247ae6c4e5acd3d1

const puppeteer = require('puppeteer');
const fs = require('fs');
const publishers = require('./publishers.js');
const categories = require('./categories');

process.setMaxListeners(20);
const dumpLocation = 'results';
const extensionLocation = 'extensions/i_dont_care_about_cookies/chr/3.3.2_0';

class Browser {
    constructor() {
        const browser = null;
    }

    async start() {
        const pathToExtension = require('path').join(__dirname, extensionLocation);
        const args = [
            //avoid headless mode detection
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-infobars',
            '--window-position=0,0',
            '--ignore-certifcate-errors',
            '--ignore-certifcate-errors-spki-list',
            '--user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36"',

            // extension loading
            `--disable-extensions-except=${pathToExtension}`,
            `--load-extension=${pathToExtension}`,

            // Viewport
            '--window-size=1920,1080'
        ];

        this.browser = await puppeteer.launch({
            args: args,
            ignoreHTTPSErrors: true,
            defaultViewport: null,
            headless: false,
        }); // Puppeteer can only generate pdf in headless mode.
    }

    async visitWithScroll(url, closeAfter = true) {
        let page = await this.browser.newPage();
        await page.goto(url, {
            waitUntil: ['load', 'domcontentloaded', 'networkidle0', 'networkidle2']
        });
        await this.autoScroll(page);
        // console.log(`total: ${this.totalHeight}`);
        let a = await page.evaluate(_ => {
            window.scrollTo(0, 0);
            //     return Promise.resolve(document.body.scrollHeight);
        });
        if (closeAfter)
            await page.close();
        return page;
    }

    async visit(url) {
        let page = await this.browser.newPage();
        await page.goto(url, {
            waitUntil: ['load', 'domcontentloaded', 'networkidle0', 'networkidle2']
        });
        return page;
    }

    async generatePicture(url, name, destination) {
        let page = await this.visitWithScroll(url, false);
        await page.screenshot({
            path: destination + '/' + name + '.png',
            fullPage: true
        });
    }

    async generatePDF(url, name, destination) {
        let page = await this.visitWithScroll(url, false);
        const pdfConfig = {
            path: destination + '/' + name + '.pdf',
            format: 'A3',
            printBackground: true,
            margin: {
                top: '0.1cm',
                bottom: '0.1cm',
                left: '0.1cm',
                right: '0.1cm'
            }
        };
        await page.emulateMediaType('screen');
        const pdf = await page.pdf(pdfConfig); // Return the pdf buffer. Useful for saving the file not to disk. 
        return pdf;
    }

    async autoScroll(page) {
        await page.evaluate(async () => {
            await new Promise((resolve, reject) => {
                var totalHeight = 0;
                var distance = 250;
                var timer = setInterval(() => {
                    var scrollHeight = document.body.scrollHeight;
                    window.scrollBy(0, distance);
                    totalHeight += distance;

                    if (totalHeight >= scrollHeight) {
                        clearInterval(timer);
                        resolve();
                    }
                }, 100);
            });
        });
    }

    async close() {
        await this.browser.close();
    }
}

async function createDirectories(categories) {
    for (let index = 0; index < categories.length; index++) {
        const category = categories[index];
        let destination = dumpLocation + '/' + category.name;
        console.log(`Creating Directory ${destination}`);
        try {
            await fs.mkdirSync(destination);
        } catch (err) {
            console.log(`mkdir exists`);
        }
    }
}

(async () => {
    let browser = new Browser();
    // await createDirectories(categories);
    let index = process.argv[2];
    const category = categories[index];
    console.log(`processing category ${category.name}`);
    let destination = dumpLocation + '/' + category.name;

    await browser.start();

    for (const url of category.urls) {
        console.log(`visiting category ${category.name} - url: ${url} `);
        try {
            await browser.visitWithScroll(url);
        } catch (err) {
            console.log(`Error in generating picture for SELLER ${url}\n ${err}`);
            continue;
        }
    }

    for (const publisher of publishers) {
        if (publisher.name == 'marketwatch' || publisher.name == 'bussinessinsider' || publisher.name == 'imdb' || 
        publisher.name == 'yahoo' || publisher.name == 'msnbc' || publisher.name == 'bussinessinsider'|| publisher.name == 'msn'
        || publisher.name == 'forbes'|| publisher.name == 'dailymotion')
            continue;
        console.log(`processing publisher ${publisher.name}`);
        // await browser.start();

        console.log(`generating picture for publisher ${publisher.url1}`);
        try {
            buffer = await browser.generatePicture(publisher.url1, publisher.name, destination);
            buffer = await browser.generatePicture(publisher.url1, publisher.name + '-2', destination);

        } catch (err) {
            console.log(`Error in generating picture for Publisher ${publisher.name}\n ${err}`);
            console.log(err);
            continue;
        }
        // await browser.close();
    }
//    await browser.close();
return;
})();
