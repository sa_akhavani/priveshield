const {Builder,Buy,Key,util} = require("selenium-webdriver");
const chrome = require('selenium-webdriver/chrome');

async function example() {

    let options = new chrome.Options();
options.addArguments("user-data-dir=/home/ali/.config/google-chrome/selenium");

let driver = await new Builder()
    .setChromeOptions(options)
    .forBrowser("chrome")
    .build();

await driver.get("http://cnn.com");

// ''' Use Navigation Timing  API to calculate the timings that matter the most '''   
 
let navigationStart = await driver.executeScript("return window.performance.timing.navigationStart")
let responseStart = await driver.executeScript("return window.performance.timing.responseStart")
let domComplete = await driver.executeScript("return window.performance.timing.domComplete")
 
// ''' Calculate the performance'''
let backendPerformance_calc = responseStart - navigationStart
let frontendPerformance_calc = domComplete - responseStart
 
console.log(`Back End: ${backendPerformance_calc}`)
console.log(`Front End: ${frontendPerformance_calc}`)

// setTimeout(() => {
//     driver.quit()    
// }, 3000);



// await driver.findElement(By.name("q").sendKeys("Selenium",Key.RETURN));
// await driver.quit();
}

example();
