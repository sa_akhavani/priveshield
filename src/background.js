import Events from './classes/EventHandlers';
const events = new Events();


//  * All callbacks are used synchronously.
//  * @return {boolean}            denotes async (true) or sync (false)
function messageHandler(request, sender, responseCallback) {
	//todo add input validation to avoid potential security threats
	let {
		name, message
	} = request;

	if (name === 'loginDetected') {
		events.onLoginDetected(message, sender, responseCallback);
		return true;
	}

	if (name === 'getTabCurrentProfile') {
		events.onGetTabCurrentProfile(message, sender, responseCallback);
		return true;
	}

	if (name === 'getProfilesInfo') {
		events.onGetProfilesInfo(message, sender, responseCallback);
		return true;
	}

	if (name === 'addProfile') {
		events.onAddProfile(message, sender, responseCallback);
		return responseCallback();
	}

	if (name === 'activateProfile') {
		events.onActivateProfile(message, sender, responseCallback);
		return responseCallback();
	}

	if (name === 'activateTmpMode') {
		events.onactivateTmpMode(message, sender, responseCallback);
		return responseCallback();
	}

	if (name === 'updateProfileCatalogue') {
		events.onUpdateProfileCatalogue(message, sender, responseCallback);
		return responseCallback();
	}

	if (name === 'getUserHistory') {
		events.onGetUserHistory(message, sender, responseCallback);
		return true;
	}

	if (name === 'switchProfile') {
		events.onSwitchProfile(message, sender, responseCallback);
		return true;
	}

	if (name === 'generateExtensionReports') {
		events.onGenerateExtensionReports(message, sender, responseCallback);
		return true;
	}

	// HANDLE PAGE EVENTS HERE
// 	if (origin === 'account_pages') {
// 		// Account pages
// 		return handleAccountPages(name, callback);
// 	}
    return false;
}

function initializeEventListeners() {

	// onBeforeNavigate -> onCommitted -> onDOMContentLoaded -> onCompleted
    // Fired when a navigation is about to occur
	chrome.webNavigation.onBeforeNavigate.addListener(events.onBeforeNavigate.bind(events));
	chrome.webNavigation.onDOMContentLoaded.addListener(events.onDOMContentLoaded.bind(events));
    // chrome.webNavigation.onBeforeNavigate.addListener(events.onBeforeNavigate.bind(events), {
    //     url: [{hostContains: '*'}]
    // });

	// chrome.webNavigation.onCompleted.addListener(events.onCompleted.bind(events));

    chrome.runtime.onInstalled.addListener(events.onInstalled.bind(events));
	chrome.runtime.onStartup.addListener(events.onStartup.bind(events));

    // chrome.cookies.onChanged.addListener(events.onCookieChanged.bind(events));

	// Fired when a new tab is created by user or internally
	chrome.tabs.onCreated.addListener(events.onTabCreated.bind(events));

	// Fires when the active tab in a window changes
	chrome.tabs.onActivated.addListener(events.onTabActivated.bind(events));


	// chrome.tabs.onUpdated.addListener(events.onTabUpdated.bind(events));

	// Fired when a tab is closed
	chrome.tabs.onRemoved.addListener(events.onTabRemoved.bind(events));

	// Fired when a message is sent from either an extension process (by runtime.sendMessage) or a content script (by tabs.sendMessage).
	chrome.runtime.onMessage.addListener(messageHandler);
}

function init() {
    initializeEventListeners();

}


init();
