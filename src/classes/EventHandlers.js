import TabDetails from './TabDetails';
import ProfileCatalogue from "./ProfileCatalogue";
import History from './History';
import {extractHostname} from '../utils/utils';
import Profile from "./Profile";

class EventHandlers {
    constructor() {
        this.tabs = [];
        this.activeTabId;
        this.profileCatalogue = new ProfileCatalogue();
    }

    async onInstalled() {
        //Check if profile catalogue exists on local storage or we should create a new catalogue
        await this.profileCatalogue.initProfileCatalogue();
        await this.profileCatalogue.generateProfilesForRegularVisits();
        await this.profileCatalogue.generateProfilesForCommonCategories();
    }

    async onStartup() {
        await this.profileCatalogue.initProfileCatalogue();
        //todo check last 7 days history
    }

    async onBeforeNavigate(details) {
        const { tabId, frameId, url } = details;
        if (url.startsWith('chrome://') || url.length < 1)
            return;
        let currentTab = this._getTab(tabId);
        // for cases that live search is active. it generates some errors so we ignore the requests in that part
        if (!currentTab)
            return;
        let oldProfile = currentTab.getProfile();
        // frameId === 0 indicates the navigation event occurred in the content window, not a subframe
        if (frameId === 0) {
            console.log(`Tab: ${tabId} navigating to URL: ${url}`);
            await this._createNewOngoingProfileIfNeeded(currentTab);
            currentTab.resetActiveTime();
            await this._updateTab(tabId, currentTab, 0);

            currentTab.url = url;
            let hostname = extractHostname(url)
            let matchingProfiles = this.profileCatalogue.fetchProfilesWithMatchingHostname(hostname)
            if (matchingProfiles.length === 0) {
                console.log('Matching 0 profiles');
                //todo if 0 -> Use nlp code and try to find a common category
                //todo check top 100 websites
                let activeProfile;
                if (this.profileCatalogue.getTmpModeStatus()) {
                    activeProfile = await this.profileCatalogue.createProfile('tmp');
                } else {
                    activeProfile = this.profileCatalogue.getActiveProfile();
                }
                let finalProfile = await this.profileCatalogue.switchProfile(oldProfile, activeProfile);
                currentTab.setProfile(finalProfile);
            }
            else if (matchingProfiles.length === 1) {
                console.log('Matching 1 profile');
                let finalProfile = await this.profileCatalogue.switchProfile(oldProfile, matchingProfiles[0]);
                currentTab.setProfile(finalProfile);
            }
            else if (matchingProfiles.length > 1) {
                console.log('Matching > 1 profiles');
                let categoryOrder = 10;
                let bestMatchingProfile;
                for (const matchingProfile of matchingProfiles) {
                    console.log(`Matching Profile: ${matchingProfile.name}`);
                    let profileCategory = this.profileCatalogue.fetchProfileCategory(matchingProfile.name);
                    let n = this.profileCatalogue.fetchProfileCategoryOrder(profileCategory);
                    if (n < categoryOrder) {
                        categoryOrder = n;
                        bestMatchingProfile = matchingProfile;
                    }
                }
                let finalProfile = await this.profileCatalogue.switchProfile(oldProfile, bestMatchingProfile);
                currentTab.setProfile(finalProfile);
            }
            //todo I think reload is required here but if you reload here it will reload the tab to the previous URL not the new one!
            // reload is disabled here to see if it is possible to do this without a reload or not
            if (currentTab.getReload() === 0)
                currentTab.setReload(1);
            else if (currentTab.getReload() === 2)
                currentTab.setReload(0);
            await this._updateTab(tabId, currentTab, 0);
        }
    }

    async onDOMContentLoaded(details) {
        const { tabId, frameId, url } = details;
        if (url.startsWith('chrome://') || url.length < 1)
            return;
        let currentTab = this._getTab(tabId);
        if (!currentTab)
            return;
        if (frameId === 0) {
            for (let i = 0; i < this.tabs.length; i++) {
                let tab = this.tabs[i];
                if (tab.id === tabId && tab.getReload() === 1) {
                    console.log('ONDOMCONTENTLOADED. RELOAD!');
                    this.tabs[i].setReload(2);
                    await TabDetails.reloadTab(tabId);
                }
            }
        }
    }

    async onTabCreated(tabData) {
        console.log(`On Tab Created: ${tabData.id}`);
        if (this._getTabIndex(tabData.id) === -1) {
            let newTab = new TabDetails(tabData);
            this.tabs.push(newTab);
            let activeProfile;
            if (this.profileCatalogue.getTmpModeStatus()) {
                activeProfile = await this.profileCatalogue.createProfile('tmp');
            } else {
                activeProfile = this.profileCatalogue.getActiveProfile();
            }
            newTab.setProfile(activeProfile);
            await this._updateTab(tabData.id, newTab);
        } else {
            console.log(`Tab Already Created. Doing nothing onTabCreated!`);
        }
    }

    async onTabActivated(tabData) {
        console.log(`Tab Activated: ${JSON.stringify(tabData)}`);
        let prevTab;
        if (this.activeTabId) {
            prevTab = this._getTab(this.activeTabId);
            if (prevTab) {
                console.log(`PREV TAB EXISTS: ${prevTab.url}`);
                let newProfile = await this._createNewOngoingProfileIfNeeded(prevTab);
                if (newProfile) {
                    if (this.profileCatalogue.checkIfSwitchProfileIsNeeded(prevTab.profile.name, newProfile.name)) {
                        prevTab.setProfile(newProfile);
                        await this._updateTab(prevTab.id, prevTab);
                    }
                }
            }
        }

        let newTab = this._getTab(tabData.tabId);
        if (newTab) {
            if (prevTab) {
                console.log('switching profile in on tab activated. prev exists')
                let finalProfile = await this.profileCatalogue.switchProfile(prevTab.getProfile(), newTab.getProfile());
                newTab.setProfile(finalProfile);
                //todo check if reload is needed
                // await TabDetails.reloadTab(newTab.getTabId());
            }
            else {
                console.log('switching profile in on tab activated. no prev')
                let finalProfile = await this.profileCatalogue.switchProfile(null, newTab.getProfile());
                newTab.setProfile(finalProfile);
                //todo check if reload is needed
                // await TabDetails.reloadTab(newTab.getTabId());
            }
            this.activeTabId = newTab.getTabId();
            await this._updateTab(tabData.tabId, newTab, 0);
        } else {
            console.log('Err: Activated Tab created before being created (before installing extension)!');
        }
    }

    async onTabRemoved(tabId) {
        console.log(`Removing TAB: ${tabId}`)
        let tab = this._getTab(tabId);
        if (tab) {
            let newProfile = await this._createNewOngoingProfileIfNeeded(tab);
            let prof = tab.profile;
            if (newProfile)
                prof = newProfile;
            if (prof.permanent)
                await this.profileCatalogue.storeProfileData(prof);
        }
        this._removeTabData(tabId)
    }

    //Ignore rn. Could be added later
    async onLoginDetected(message, sender, callback) {
        console.log(`On Login Detected`);
        /*
        let tabId = sender.tab.id;

        //If tab is already created because of login detection
        if (this.tabs[this._getTabIndex(tabId)].createdByExtension) {
            callback({
                message: 'GG;'
            });
        } else {
            // create new tab
            let p = this.profileCatalogue.getPersistentProfile();
            let t = await TabDetails.createTab(sender.tab.url, false);
            console.log(`Created Tab in function!!!!!`);

            let idx = this._getTabIndex(t.id)

            if (idx === -1) {
                //It must not come into this part, just putting it here to make sure nothings bad happens
                console.log('on login detected. tab not in tabs[]. creating new one.')
                let newTab = new TabDetails(t, true);
                newTab.setProfile(p);
                this.tabs.push(newTab);
            } else {
                console.log('on login detected. tab exists. switching profile')
                let prevProfile = this.tabs[idx].profile;
                this.tabs[idx].setProfile(p);
                this.tabs[idx].createdByExtension = true;
                if (!prevProfile.permanent) {
                    this.profileCatalogue.removeProfile(prevProfile.id);
                }
            }
            await TabDetails.activateTab(t.id);
            TabDetails.removeTab(tabId);

            callback({
                message: 'EZ;'
            });
        }
        */
    }

    onGetTabCurrentProfile(message, sender, callback) {
        let tabId = message.id;
        let tabIndex = this._getTabIndex(tabId)
        if (tabIndex >= 0) {
            callback({
                message: this.tabs[this._getTabIndex(tabId)].profile.name
            });
        } else {
            callback({
                message: null
            });
        }
    }

    //todo check use case
    onGetProfilesInfo(message, sender, callback) {
        callback({
            message: {
                permanentProfilesList: this.profileCatalogue.getNonTmpProfiles(),
                activeProfile: this.profileCatalogue.getActiveProfile()
            }
        });
    }

    async onAddProfile(message, sender, callback) {
        await this.profileCatalogue.addProfile(message);
    }

    async onActivateProfile(message) {
        let currentTab = this._getTab(this.activeTabId);
        let profile = this.profileCatalogue.getProfileWithName(message)
        this.profileCatalogue.activateProfile(profile);
        await this.profileCatalogue.switchProfile(currentTab.getProfile(), profile);
        currentTab.setProfile(profile);
        await this._updateTab(this.activeTabId, currentTab, 1);
    }

    async onactivateTmpMode() {
        let currentTab = this._getTab(this.activeTabId);
        this.profileCatalogue.activateTmpMode();
        let activeProfile = await this.profileCatalogue.createProfile('tmp');
        await this.profileCatalogue.switchProfile(currentTab.getProfile(), activeProfile);
        currentTab.setProfile(activeProfile);
        await this._updateTab(this.activeTabId, currentTab, 1);
    }

    onUpdateProfileCatalogue(message) {
        this.profileCatalogue.updateNameAndWebsites(message)
    }

    async onGetUserHistory(message, sender, callback) {
        let history = new History();
        let results = await history.getUserHistory();
        callback({
            message: results
        });
    }

    onGenerateExtensionReports() {
        let report = JSON.stringify(this.profileCatalogue);
        report += '\n' + '------------' + '\n';
        for (const profile of this.profileCatalogue.profileCatalogue) {
            report += `${profile.name}: {\n`
            report += `${JSON.stringify(profile.cookies)}`;
            report += '}\n';
        }
        // report += JSON.stringify(this.tabs);
        this._saveFile(report);
    }

    //todo copy previous profile information here
    async _createNewOngoingProfileIfNeeded(tab) {
        if (!tab)
            return;
        console.log(`_createNewOngoingProfileIfNeeded - tab.url: ${tab.url}`);
        if(tab.url && tab.url.length > 0) {
            let shouldCreateProfile = tab.checkIfNeedNewProfile(Date.now());
            if (shouldCreateProfile && tab.url.length > 3) {
                let result = await this.profileCatalogue.createProfile('session', extractHostname(tab.url));
                if (result.flag) {
                    console.log(`New profile created! ${result.newProfile.name}`);
                    return result.newProfile;
                }
            }
        }
    }

    _saveFile(fileDataString) {
        let vLink = document.createElement('a'),
            vBlob = new Blob([fileDataString], {type: "octet/stream"}),
            vName = 'profile_manager_extension_report.json',
            vUrl = window.URL.createObjectURL(vBlob);
        vLink.setAttribute('href', vUrl);
        vLink.setAttribute('download', vName);
        vLink.click();
    }

    _removeTabData(tabId) {
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].getTabId() === tabId) {
                if (!this.tabs[i].profile.permanent)
                    this.profileCatalogue.removeProfile(this.tabs[i].profile.id);
                this.tabs.splice(i, 1);
            }
        }
    }

    _getTabIndex(tabId) {
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].getTabId() === tabId)
                return i;
        }
        return -1;
    }

    _getTab(tabId) {
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].getTabId() === tabId)
                return this.tabs[i];
        }
    }

    async _updateTab(tabId, newTab, reloadRequired = 0) {
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].getTabId() === tabId) {
                this.tabs[i] = newTab;
                if (reloadRequired) {
                    await TabDetails.reloadTab(newTab.getTabId());
                }
            }
        }
    }
}

export default EventHandlers;