import {extractHostname} from '../utils/utils';

class History {
    constructor() {
        this.urlToCount = {};
    }

    async getUserHistory(upto, howLongBeforeMicroseconds) {
        let historyStartTime = upto - howLongBeforeMicroseconds;
        let p = new Promise( (resolve, reject) => {
            chrome.history.search({
                'text': '',
                'startTime': historyStartTime
            }, function (historyItems) {
                resolve(historyItems);
            });
        });
        let historyItems = await p;
        for (let i = 0; i < historyItems.length; ++i) {
            let url = historyItems[i].url;
            let hostname = extractHostname(url);
            if (this.urlToCount[hostname])
                this.urlToCount[hostname] += historyItems[i].visitCount;
            else
                this.urlToCount[hostname] = historyItems[i].visitCount;
        }
        return this.urlToCount;
    }

    // _onAllVisitsProcessed() {
    //     let urlArray = [];
    //     for (let element of this.urlToCount) {
    //         let url = (Object.keys(element))[0];
    //         urlArray.push({
    //             url: url,
    //             count: this.urlToCount[url]
    //         });
    //     }
    //     // urlArray.sort(function (a, b) {
    //     //     return this.urlToCount[b] - this.urlToCount[a];
    //     // });
    //     // return urlArray.slice(0, 10);
    //     return urlArray;
    // }
}

export default History;