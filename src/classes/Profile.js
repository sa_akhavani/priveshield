class Profile {
    constructor(name, permanent = true, websites = [], id, cookies) {
        this.name = name;
        if (!id)
            this.id = this._generateId();
        else
            this.id = id;
        this.websites = websites;
        if (!cookies)
            this.cookies = [];
        else
            this.cookies = cookies;
        this.localStorage;
        this.sessionStorage;
        this.permanent = permanent;
        this.expiryDate;
        if (!permanent) {
            let now = new Date()
            now.setTime(now.getTime() + 2 * 3600000); // Set expiry time to 2 hours from now.
            this.expiryDate = now;
        }
    }

    addWebsites(newWebsites) {
        for (const hostname of newWebsites) {
            if (this.websites.indexOf(hostname) !== -1)
                this.websites.push(hostname);
        }
    }

    _generateId() {
        let res = '';
        for(let i = 0; i < 10; i++){
            const rnd = Math.round(Math.random() * 10);
            res += rnd;
        }
        return res;
    }
}

export default Profile;