import simpleProfileCatalogue from "../utils/profileCatalogue";
import Profile from "./Profile";
import StorageManager from "./StorageManager";
import History from "./History";
import {extractHostname} from '../utils/utils';
import commonCategories from "../utils/commonCategories";

class ProfileCatalogue {

    constructor() {
        this.profileCatalogue = [];
        this.activeProfile;
        this.tmpMode = false;
    }

    async initProfileCatalogue() {
        let p = new Promise( (resolve, reject) => {
            chrome.storage.local.get('profileCatalogue', (result) => {
                let res = result.profileCatalogue
                if (!res || Object.keys(res).length === 0) {
                    for (const prof of simpleProfileCatalogue) {
                        this.profileCatalogue.push(new Profile(prof.name, prof.permanent, prof.websites, prof.id, prof.cookies))
                    }
                    chrome.storage.local.set({
                        profileCatalogue: this.profileCatalogue
                    }, () => {
                        this.activeProfile = this.profileCatalogue[0];
                        console.log('Profile Catalogue Initialized From Scratch');
                        resolve();
                    });
                } else {
                    for (const prof of res) {
                        this.profileCatalogue.push(new Profile(prof.name, prof.permanent, prof.websites, prof.id, prof.cookies))
                    }
                    this.activeProfile = this._fetchDefaultProfile();
                    console.log('Profile Catalogue Already Existed On the Storage');
                    resolve();
                }
            });
        });
        return await p;
    }

    async generateProfilesForRegularVisits() {
        let history = new History();
        let days = 30;
        let daysInMicroseconds = 1000 * 60 * 60 * 24 * days;
        let visitThreshold = 5;
        let lastMonthHistory = await history.getUserHistory((new Date).getTime(), daysInMicroseconds);
        console.log(`Last Month History:\n ${JSON.stringify(lastMonthHistory)}`);

        for (const url of Object.keys(lastMonthHistory)) {
            let host = extractHostname(url);
            if (lastMonthHistory[host] > visitThreshold) {
                let profileName = 'regular-' + host;
                if (this._isProfileNameUnique(profileName)) {
                    let profile = new Profile(profileName, true, [host]);
                    this.profileCatalogue.push(profile);
                } else {
                    this._addWebsitesToExistingProfile(profileName, [host]);
                }
            }
        }
        await this._syncProfileCatalogue();
    }

    async generateProfilesForCommonCategories() {
        for (const category of commonCategories) {
            let profileName = 'common-' + category.split(' ').join('_');
            if (this._isProfileNameUnique(profileName)) {
                let profile = new Profile(profileName, true);
                this.profileCatalogue.push(profile);
            }
        }
        this._syncProfileCatalogue();
    }

    getActiveProfile() {
        return this.activeProfile;
    }

    getProfileWithName(profName) {
        for (const prof of this.profileCatalogue) {
            if (prof.name === profName)
                return prof;
        }
    }

    activateProfile(profile) {
        this.activeProfile = profile;
        this.tmpMode = false;
    }

    activateTmpMode() {
        this.activeProfile = this._fetchDefaultProfile();
        this.tmpMode = true;
    }

    getTmpModeStatus() {
        return this.tmpMode;
    }

    getPersistentProfile() {
        for (const p of this.profileCatalogue) {
            if (p.name === 'Persistent (Automatic)')
                return p;
        }
    }

    getNonTmpProfiles() {
        let profs = [];
        for (const p of this.profileCatalogue) {
            if (p.permanent)
                profs.push(p);
        }
        return profs;
    }

    async createProfile(profileType, hostname) {
        console.log(`Creating Profile for hostname ${hostname} with type: ${profileType}`);
        let newProfile;
        if (profileType === 'tmp') {
            newProfile = new Profile(this._generateRandomName('tmp-'), false);
        }
        else if (profileType === 'interact') {
            newProfile = new Profile(`interact-${hostname}`, true, [hostname]);
        }
        else if (profileType === 'session') {
            newProfile = new Profile(`session-${hostname}`, true, [hostname]);
        }
        let flag = 1;
        for (let i = 0; i < this.profileCatalogue.length; i++) {
            if (this.profileCatalogue[i].name === newProfile.name) {
                console.log(`WARNING! Tried to create a profile with already existing name. Profile name: ${newProfile.name}. Returning the older profile`)
                flag = 0;
                let profile = this.profileCatalogue[i];
                //todo profile might not be Profile!
                profile.addWebsites([hostname]);
                this.profileCatalogue[i] = profile;
            }
        }
        if (flag)
            this.profileCatalogue.push(newProfile);
        await this._syncProfileCatalogue();
        return {newProfile, flag};
    }

    async addProfile(profileData) {
        let name = profileData.name;
        let websites = profileData.websites.split(',');
        let finalWebsites = [];
        for (let i = 0; i < websites.length; i++) {
            let str = websites[i].trim();
            if (str)
                finalWebsites.push(websites[i]);
        }

        let profile = new Profile(name, true, finalWebsites)
        this.profileCatalogue.push(profile);
        this._syncProfileCatalogue();
    }

    removeProfile(profileId) {
        for (let i = 0; i < this.profileCatalogue.length; i++) {
            if (profileId === this.profileCatalogue[i].id) {
                this.profileCatalogue.splice(i, 1);
            }
        }
        this._syncProfileCatalogue();
    }

    updateNameAndWebsites(updatedProfileCatalogue) {
        for (const updatedProf of updatedProfileCatalogue) {
            for (let i = 0; i < this.profileCatalogue.length; i++) {
                if (this.profileCatalogue[i].id === updatedProf.id) {
                    console.log(JSON.stringify(updatedProf));
                    this.profileCatalogue[i].name = updatedProf.name;
                    this.profileCatalogue[i].websites = updatedProf.websites;
                }
            }
        }
        this._syncProfileCatalogue();
    }

    async switchProfile(prevProfile, newProfile) {
        console.log(`Switching profiles!`);
        if (prevProfile) {
            console.log(`Switching Profiles! prev: ${prevProfile.name} ---- new: ${newProfile.name}`);
            if (prevProfile.id === newProfile.id) {
                return prevProfile;
            }
            if (!this.checkIfSwitchProfileIsNeeded(prevProfile.name, newProfile.name)) {
                console.log('NO Switch because of Priority!');
                return prevProfile;
            }
        }

        if (prevProfile) {
            await this.storeProfileData(prevProfile);
        }
        await this._clearProfileData();
        await this._loadProfileData(newProfile);
        return newProfile;
    }

    checkIfSwitchProfileIsNeeded(prevProfileName, newProfileName) {
        if (prevProfileName === newProfileName)
            return 0;
        let prevIndex = prevProfileName.indexOf('-');
        let newIndex = newProfileName.indexOf('-');
        if (prevIndex < 0 || newIndex < 0)
            return 1;
        let str1 = prevProfileName.substr(prevIndex + 1);
        let str2 = newProfileName.substr(newIndex + 1);
        if (str1 !== str2) {
            console.log(`Strings do not MATCH! ${prevProfileName} |||| ${newProfileName}`)
            return 1;
        }
        let prevOrder = this.fetchProfileCategoryOrder(this.fetchProfileCategory(prevProfileName));
        let newOrder = this.fetchProfileCategoryOrder(this.fetchProfileCategory(newProfileName));
        if (newOrder < prevOrder)
            return 1;
        return 0;
    }

    async storeProfileData(prevProfile) {
        let allCookies = await StorageManager.getAllCookies();
        let allLocalStorage = await StorageManager.getAllItemsInLocalStorage();
        let allSessionStorage = await StorageManager.getAllItemsInSessionStorage();
        for (let i = 0; i < this.profileCatalogue.length; i++) {
            if (this.profileCatalogue[i].id === prevProfile.id) {
                this.profileCatalogue[i].cookies = allCookies;
                this.profileCatalogue[i].localStorage = allLocalStorage;
                this.profileCatalogue[i].sessionStorage = allSessionStorage;
            }
        }
        await this._syncProfileCatalogue();
    }

    fetchProfilesWithMatchingHostname(hostname) {
        let matchingProfiles = [];
        for (const profile of this.profileCatalogue) {
            for (const website of profile.websites) {
                if (website === hostname) {
                    matchingProfiles.push(profile)
                    break;
                }
            }
        }
        return matchingProfiles;
    }

    fetchProfileCategory(profileName) {
        if (profileName.startsWith('manual-'))
            return 'manual';
        else if (profileName.startsWith('regular-'))
            return 'regular';
        else if (profileName.startsWith('interact-'))
            return 'interact';
        else if (profileName.startsWith('session-'))
            return 'session';
        else if (profileName.startsWith('common-'))
            return 'common';
        else if (profileName.startsWith('tmp-'))
            return 'tmp';
        else if (profileName.startsWith('default'))
            return 'default';
        return 'undefinedCategory'
    }

    fetchProfileCategoryOrder(category) {
        switch (category) {
            case 'manual':
                return 0;
            case 'regular':
                return 1;
            case 'interact':
                return 2;
            case 'session':
                return 3;
            case 'common':
                return 4;
            case 'tmp':
                return 5;
            case 'default':
                return 10;
            default:
                return 9;
        }
    }

    _fetchDefaultProfile() {
        for (const profile of this.profileCatalogue) {
            if (profile.name === 'default' && profile.id === '0')
                return profile;
        }
    }

    _isProfileNameUnique(newName) {
        for (const profile of this.profileCatalogue) {
            if (profile.name === newName)
                return false;
        }
        return true;
    }

    _addWebsitesToExistingProfile(profileName, newWebsites) {
        for (let i = 0; i < this.profileCatalogue.length; i++) {
            if (this.profileCatalogue[i].name === profileName) {
                this.profileCatalogue[i].websites.concat(newWebsites);
            }
        }
    }

    async _clearProfileData() {
        await StorageManager.deleteAllCookies();
        StorageManager.clearLocalStorage();
        StorageManager.clearSessionStorage();
        StorageManager.clearAppCache();
        StorageManager.clearCache();
        StorageManager.clearCacheStorage();
    }

    async _loadProfileData(newProfile) {
        //todo find a way to restore cache storage
        for (let i = 0; i < newProfile.cookies.length; i++) {
            await StorageManager.setCookie(newProfile.cookies[i])
        }
        StorageManager.setLocalStorage(newProfile.localStorage);
        StorageManager.setSessionStorage(newProfile.sessionStorage);
    }

    async _syncProfileCatalogue() {
        let permanentProfiles = [];
        for (let i = 0; i < this.profileCatalogue.length; i++) {
            if (this.profileCatalogue[i].permanent)
                permanentProfiles.push(this.profileCatalogue[i]);
        }
        chrome.storage.local.set({
            profileCatalogue: permanentProfiles
        }, () => {
            console.log('Profile Catalogue Synced');
        });
    }

    static _fetchCurrentProfileFromStorage() {
        chrome.storage.local.get('profileCatalogue', (result) => {
            return result.profileCatalogue;
        });
    }

    _generateRandomName(prefix = '') {
        const num = 8;
        let res = prefix;
        for(let i = 0; i < num; i++){
            const rnd = Math.round(Math.random() * 10);
            res += rnd;
            // const random = Math.floor(Math.random() * 27);
            // res += String.fromCharCode(97 + random);
        }
        return res;
    }
}

export default ProfileCatalogue;