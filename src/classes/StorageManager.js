class StorageManager {
    constructor() {
    }

    static async getAllCookies() {
        let p = new Promise( (resolve, reject) => {
            chrome.cookies.getAll({}, (cookies) => {
                resolve(cookies);
            });
        });
        return await p;
    }

    static async deleteAllCookies() {
        let allCookies = await this.getAllCookies();
        // console.log(`Fetched All ${allCookies.length} Cookies`);
        this._deleteCookies(allCookies);
        // console.log(`Deleted All ${allCookies.length} Cookies`);
    }

    static setLocalStorage(newLocalStorage) {
        if (!newLocalStorage)
            return;
        for (let i = 0; i < newLocalStorage.length; i++) {
            localStorage.setItem(newLocalStorage[i]);
        }
    }

    static setSessionStorage(newSessionStorage) {
        if (!newSessionStorage)
            return;
        for (let i = 0; i < newSessionStorage.length; i++) {
            sessionStorage.setItem(newSessionStorage[i]);
        }
    }

    static clearSessionStorage() {
        sessionStorage.clear();
    }

    static clearLocalStorage() {
        localStorage.clear();
    }

    static async getAllItemsInLocalStorage() {
        let values = [];
        let keys = Object.keys(localStorage);
        for (let i = 0; i < keys.length; i++) {
            values.push(localStorage.getItem(keys[i]))
        }
        return values;
    }

    static async getAllItemsInSessionStorage() {
        let values = [];
        let keys = Object.keys(sessionStorage);
        for (let i = 0; i < keys.length; i++) {
            values.push(sessionStorage.getItem(keys[i]))
        }
        return values;
    }

    static clearCache() {
        chrome.browsingData.removeCache({}, () => {
            // console.log('Removed Cache')
        });
    }

    static clearAppCache() {
        chrome.browsingData.removeAppcache({}, () => {
            // console.log('Removed App Cache')
        });
    }

    static clearCacheStorage() {
        chrome.browsingData.removeCacheStorage({}, () => {
            // console.log('Removed Cache Storage')
        });
    }

    cookieMatch(c1, c2) {
        return (c1.name === c2.name) && (c1.domain === c2.domain) &&
            (c1.hostOnly === c2.hostOnly) && (c1.path === c2.path) &&
            (c1.secure === c2.secure) && (c1.httpOnly === c2.httpOnly) &&
            (c1.session === c2.session) && (c1.storeId === c2.storeId);
    }

    static async setCookie(cookie) {
        // console.log(`Setting Cookie: ${JSON.stringify(cookie)}`);
        //todo check hostonly=true
        //todo there are some httponly and secure cookies which lead to err!
        let newCookie = {
            name: cookie.name,
            storeId: cookie.storeId,
            value: cookie.value,
            path: cookie.path,
            httpOnly: cookie.httpOnly,
            secure: cookie.secure,
            sameSite: cookie.sameSite,
        };
        if(!cookie.hostOnly)
            newCookie.domain = cookie.domain;
        if(!cookie.session)
            newCookie.expirationDate = cookie.expirationDate;
        newCookie.url = ((cookie.secure) ? "https://" : "http://") + cookie.domain.replace(/^\./, ""); //Cookie Url should not start with .

        let p = new Promise( (resolve, reject) => {
            chrome.cookies.set(newCookie, function (res, err) {
                resolve(res)
            });
        });
        return await p;
    }

    static _deleteCookies(cookies) {
        // console.log(`Deleting " ${cookies.length} + " Cookies`);
        for(var i=0; i<cookies.length; i++) {
            let curr = cookies[i];
            var url = ((curr.secure) ? "https://" : "http://") + curr.domain + curr.path;
            this._deleteCookie(url, curr.name, curr.storeId);
        }
    }

    static _deleteCookie(url, name, storeId) {
        chrome.cookies.remove({
            "url": url,
            "name": name,
            "storeId": storeId
        });
    }

}

export default StorageManager;