//todo fix active threshold time to 5 minutes. it is less than that for test purposes
let ACTIVE_THRESHOLD = 5 * 60 * 1000 // 5 Minutes
// let ACTIVE_THRESHOLD =  10 * 1000 // 10 secs

class TabDetails {
    constructor(tabData, createdByExtension = false) {
        this.profile;
        this.id = tabData.id;
        this.url = tabData.url;
        this.activeTime = 0;
        this.lastActiveTimestamp = Date.now();
        this.createdByExtension = createdByExtension;
        this.shouldReload = 0;
    }

    setReload(value) {
        this.shouldReload = value;
    }

    setProfile(profile) {
        this.profile = profile;
    }

    getProfile() {
        return this.profile;
    }

    getTabId() {
        return this.id;
    }

    getReload() {
        return this.shouldReload;
    }

    resetActiveTime() {
        this.activeTime = 0;
    }

    setLastActiveTimestamp(timestamp) {
        this.lastActiveTimestamp = timestamp;
    }

    // Return 1 if we need to generate a new profile for this active session. Otherwise return 0
    checkIfNeedNewProfile(timestamp) {
        let duration = timestamp - this.lastActiveTimestamp;
        this.lastActiveTimestamp = timestamp;
        this.activeTime += duration;
        if (this.activeTime > ACTIVE_THRESHOLD) {
            return 1;
        }
        return 0;
    }

    async getTab(tabId) {
        return new Promise((resolve, reject) => {
            chrome.tabs.get(tabId, function (tab) {
                resolve(tab);
            });
        });
    }

    static async reloadTab(tabId) {
        return await new Promise( (resolve, reject) => {
            chrome.tabs.reload(tabId, {
                bypassCache: true
            }, (tab) => {
                console.log(`Tab: ${tabId} Reloaded!`);
                resolve(tab)
            });
        });
    }

    static async removeTab(tabId) {
        chrome.tabs.remove(tabId, () => {});
    }

    static async createTab(url, active = true) {
        return await new Promise( (resolve, reject) => {
            chrome.tabs.create({
                active: active,
                url: url,
            }, (tab) => {
                resolve(tab)
            });
        });
    }

    static async activateTab(tabId) {
        return await new Promise( (resolve, reject) => {
            chrome.tabs.update(tabId, {
                active: true
            }, () => {
                resolve();
            });
        });
    }
}

export default TabDetails;