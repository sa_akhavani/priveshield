document.addEventListener('DOMContentLoaded', function () {
    let submitBtn = document.getElementById('addProfile');
    submitBtn.addEventListener('click', addProfile, false);
}, false);

function addProfile() {
    let profileName = document.getElementById('profileInput').value;
    let websites = document.getElementById('websiteInput').value;
    let profileData = {
        name: profileName,
        websites: websites
    };

    chrome.runtime.sendMessage({
        name: 'addProfile',
        message: profileData
    }, () => {
        chrome.tabs.getSelected(null, function(tab) {
            chrome.tabs.remove(tab.id);
        });
    });
}