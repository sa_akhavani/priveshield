import Detectors from "./detectors";

console.log('I run on all pages!');
document.body.onload = function () {
    if (document.readyState === 'complete') {
        //     chrome.runtime.sendMessage({
        //         name: 'switchProfile'
        //     }, function (response) {
        //         console.log(JSON.stringify(response.message));
        //     });
        // }
        setTimeout(() => { // Making sure everything is loaded!
            if (Detectors.detectLoginForm(document)) {
                chrome.runtime.sendMessage({
                    name: 'loginDetected'
                }, function (response) {
                    console.log(JSON.stringify(response.message));
                });
            }
        }, 1000)
    }
}
