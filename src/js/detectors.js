class Detectors {

    static detectLoginForm(document) {
        console.log('Running Login Detector!')
        let hasPassword = false;
        let hasUsername = false;
        let hasEmail = false;
        let hasTel = false;
        let inputs = document.getElementsByTagName('input');
        for (let i = 0; i < inputs.length; i++) {
            let input = inputs[i];
            if (input.type === 'password')
                hasPassword = true;
            if (input.type === 'text')
                hasUsername = true;
            if (input.type === 'email')
                hasEmail = true;
            if (input.type === 'tel')
                hasTel = true;
        }
        console.log(hasPassword)
        console.log(hasUsername)
        console.log(hasEmail)
        console.log(hasTel)

        return hasPassword;
    }

}

export default Detectors;
