document.addEventListener('DOMContentLoaded', function () {
  // const bg = chrome.extension.getBackgroundPage();
  let editProfileBtn = document.getElementById('editProfiles');
  let addProfileBtn = document.getElementById('addProfile');
  let profilesDiv = document.getElementById('profile-group');
  let currentProfileDiv = document.getElementById('current-profile');
  let settingsGroup = document.getElementById('settings-group');
  let saveReportButton = document.getElementById('saveReport');

  chrome.tabs.getSelected(null, function(tab) {
    chrome.runtime.sendMessage({
      name: 'getTabCurrentProfile',
      message: tab
    }, function (response) {

      let profileName = response.message;
      const currentProfLabel = document.createElement('label');
      currentProfLabel.innerHTML = `Active Profile: ${profileName}`;
      currentProfLabel.id = 'currentProf';
      currentProfLabel.className = 'popupLabel';
      currentProfileDiv.appendChild(currentProfLabel)
    });
  });

  chrome.runtime.sendMessage({
    name: 'getProfilesInfo',
    message: 'getProfilesInfo'
  }, function (response) {

    let profileList = response.message.permanentProfilesList;
    let activeProfile = response.message.activeProfile;

    for (const prof of profileList) {
      const profileBtn = document.createElement('button')
      profileBtn.textContent = `${prof.name}`;
      profileBtn.id = 'profileBtn'

      if (activeProfile && prof.name === activeProfile.name)
        profileBtn.className += 'profileButton activeProfile';
      else
        profileBtn.className += 'profileButton inactiveProfile';

      profileBtn.addEventListener('click', activateProfile, false)
      profilesDiv.appendChild(profileBtn)
    }
  });

  const tmpBtn = document.createElement('button')
  tmpBtn.textContent = `Activate TMP Mode`;
  tmpBtn.id = 'activateTmpBtn'
  tmpBtn.className += 'activateTmpBtn';
  tmpBtn.addEventListener('click', activateTmpMode, false)
  settingsGroup.appendChild(tmpBtn)


  editProfileBtn.addEventListener('click', () => {
    chrome.tabs.create({
      url: './settings.html'
    })
  }, false);

  addProfileBtn.addEventListener('click', () => {
    chrome.tabs.create({
      url: './addProfile.html'
    });
  }, false);

  saveReportButton.addEventListener('click', () => {
    chrome.runtime.sendMessage({
      name: 'generateExtensionReports',
      message: 'nothing'
    }, () => {});
  });

}, false);

function activateProfile() {
  let allProfiles = document.getElementsByClassName('profileButton')
  for (const profile of allProfiles)
    profile.className = 'profileButton inactiveProfile';

  this.className = 'profileButton activeProfile';

  chrome.runtime.sendMessage({
    name: 'activateProfile',
    message: this.textContent
  }, function () {
    //  tab needs to get reloaded so that popup shows updated info. this is done in background not here.
  });
}

function activateTmpMode() {
  chrome.runtime.sendMessage({
    name: 'activateTmpMode',
    message: 'activateTmpMode'
  }, function () {
  //  tab needs to get reloaded so that popup shows updated info. this is done in background not here.
  });
}