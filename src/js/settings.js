document.addEventListener('DOMContentLoaded', function () {
    let profiles;
    chrome.runtime.sendMessage({
        name: 'getProfilesInfo',
        message: 'getProfilesInfo'
    }, function (response) {

        let profList = response.message.permanentProfilesList;
        let profilesList = document.getElementById('profiles-list');

        for (const profile of profList) {
            let profileDiv = document.createElement('div');
            profileDiv.className = 'profileSection';

            let profileNameInput = document.createElement('input');
            profileNameInput.value = `${profile.name}`;
            profileNameInput.className = 'profileInputClass settingField'

            let profileId = document.createElement('input');
            profileId.type = 'hidden';
            profileId.value = `${profile.id}`;
            profileId.className = 'settingField'

            // let labelDiv = document.createElement('div');
            let profileLabel = document.createElement('label');
            profileLabel.innerHTML = `Websites in Profile ${profile.name}: (Comma Separated)`
            profileLabel.className = 'settingField'
            // labelDiv.appendChild(profileLabel);

            let websitesInput = document.createElement('input');
            websitesInput.className = 'websiteInput settingField'
            websitesInput.id = profile.name;
            let websites = '';
            for (const website of profile.websites) {
                websites += website;
                websites += ', ';
            }
            websitesInput.value = websites;
            //Adjust width of input field to its input
            websitesInput.style.width = websitesInput.value.length + "ch";

            profileDiv.appendChild(profileNameInput);
            profileDiv.appendChild(profileId);
            profileDiv.appendChild(profileLabel);
            profileDiv.appendChild(websitesInput);

            profilesList.appendChild(profileDiv);
        }
    });

    let saveBtn = document.getElementById('saveProfiles');
    saveBtn.addEventListener('click', saveProfiles, false);

}, false);

function saveProfiles() {
    let profileCatalogue = [];
    let profiles = document.getElementsByClassName('profileSection');
    for (const profile of profiles) {
        for (let i = 0; i < profile.children.length; i++) {
            let profileName = profile.children[0].value;
            let profileId = profile.children[1].value;
            let profileWebsites = profile.children[3].value;
            let websites = profileWebsites.split(',');
            let finalWebsites = [];
            for (let j = 0; j < websites.length; j++) {
                websites[j] = websites[j].trim();
                if (websites[j])
                    finalWebsites.push(websites[j]);
            }
            profileCatalogue.push({
                name: profileName,
                id: profileId,
                websites: finalWebsites
            });
        }
    }
    chrome.runtime.sendMessage({
        name: 'updateProfileCatalogue',
        message: profileCatalogue
    }, () => {
        chrome.tabs.getSelected(null, function(tab) {
            chrome.tabs.remove(tab.id);
        });
    });
}