export default [{
    id: '0',
    name: 'default',
    websites: [],
    cookies: [],
    localStorage: [],
    sessionStorage: [],
    permanent: true,
    creation: 'manual'
}, {
    id: '100',
    name: 'Persistent (Automatic)',
    websites: [],
    cookies: [],
    localStorage: [],
    sessionStorage: [],
    permanent: true,
    creation: 'automatic'
}];
