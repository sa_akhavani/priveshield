function getTab(tab_id, callback, error) {
	chrome.tabs.get(tab_id, (tab) => {
		if (chrome.runtime.lastError) {
			log('getTab', chrome.runtime.lastError.message);
			if (error && typeof error === 'function') {
				error(chrome.runtime.lastError);
			}
		} else if (tab && typeof callback === 'function') {
			callback(tab);
		}
	});
}

function removeSubDomain(v) {
	let is_co = v.match(/\.co\./)
	v = v.split('.')
	v = v.slice(is_co ? -3 : -2)
	v = v.join('.')
	console.log(v)
	return v
}

function extractHostname(url) {
	if (!url.includes('http://') && !url.includes('https://'))
		url = 'http://' + url;

	const { hostname } = new URL(url);
	let final;
	let a = hostname.split('.');
	if (a.length >= 3) {
		a.shift();
		final = a.join('.');
	} else {
		final = hostname;
	}
	return final;
}

export {getTab, removeSubDomain, extractHostname};