var webpack = require("webpack"),
    path = require("path"),
    env = require("./webpack/env"),
    CleanWebpackPlugin = require("clean-webpack-plugin").CleanWebpackPlugin,
    CopyWebpackPlugin = require("copy-webpack-plugin"),
    HtmlWebpackPlugin = require("html-webpack-plugin"),
    WriteFilePlugin = require("write-file-webpack-plugin");

// load the secrets
var alias = {};

var fileExtensions = ["jpg", "jpeg", "png", "gif", "eot", "otf", "svg", "ttf", "woff", "woff2"];

var options = {
    mode: process.env.NODE_ENV || "development",
    entry: {
        popup: path.join(__dirname, "src", "js", "popup.js"),
        options: path.join(__dirname, "src", "js", "options.js"),
        background: path.join(__dirname, "src", "background.js"),
        content: path.join(__dirname, "src", "js", "content.js"),
        settings: path.join(__dirname, "src", "js", "settings.js"),
        addProfile: path.join(__dirname, "src", "js", "addProfile.js"),
    },
    output: {
        path: path.join(__dirname, "build"),
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                loader: "style-loader!css-loader",
                exclude: /node_modules/
            },
            {
                test: new RegExp('.(' + fileExtensions.join('|') + ')$'),
                loader: "file-loader?name=[name].[ext]",
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: "html-loader",
                exclude: /node_modules/
            },
        ]
    },
    resolve: {
        alias: alias
    },
    plugins: [
        // clean the build folder
        new CleanWebpackPlugin(),
        // expose and write the allowed env vars on the compiled bundle
        new webpack.EnvironmentPlugin(["NODE_ENV"]),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "src", "html", "popup.html"),
            filename: "popup.html",
            chunks: ["popup"]
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "src", "html", "settings.html"),
            filename: "settings.html",
            chunks: ["settings"]
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "src", "html", "addProfile.html"),
            filename: "addProfile.html",
            chunks: ["addProfile"]
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "src", "html", "background.html"),
            filename: "background.html",
            chunks: ["background"]
        }),
        new CopyWebpackPlugin([
            {from:path.join(__dirname, "src", "images"),to:'images'}
        ]),
        new CopyWebpackPlugin([
            {from:path.join(__dirname, "src", "manifest.json"),to:'./'}
        ]),
        new CopyWebpackPlugin([
            {from:path.join(__dirname, "src", "js", "jquery.min.js"),to:'./'}
        ]),
        new CopyWebpackPlugin([
            {from:path.join(__dirname, "src", "css", "style.css"),to:'./'}
        ]),
        new WriteFilePlugin()
    ]
};

if (env.NODE_ENV === "development") {
    // options.devtool = "cheap-module-eval-source-map";
    options.devtool = "cheap-module-source-map";
}

module.exports = options;
